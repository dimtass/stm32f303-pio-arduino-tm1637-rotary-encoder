Rotary encoder and TM1637 display on STM32F303 using Arduino SDK
----

> Note: there's a blog post for this code [here](https://www.stupid-projects.com/comparing-a-simple-project-with-a-rotary-encoder-and-a-7-segment-tm1637-display-with-arduino-and-ll-libs-on-stm32f303/)

This repo is meant only for testing and comparing the performance of
a simple rotary knob and TM1637 project using the Arduino SDK against
the LL (Low-Level library) for STM32. You can find the LL project code
for the same MCU (STM32F303) [here](https://bitbucket.org/dimtass/stm32f303-tm1637-rotary-encoder/src/master/).

The project is made using PlatformIO (PIO) on Visual Studio Code (VSC);
therefore you need to open this folder with VSC using the PIO plugin.

The library for the TM1637 is the avishorp Arduino library that is located
[here](https://github.com/avishorp/TM1637). The same library is ported for
the LL project, so the code is comparable.

You can control the min/max value of the rotary encoder in the
`source/src/main.c` by changin the limits in the `rep_init()`
function. Currently the min is set to `-100` and max to `100`.

```cpp
rep_init(&pot1, 0, -100, 100, 1, 0, &rotary_encoder_cbk);
```

One of the pins of the rotary encoder is attached to an EXTI interrupt.
You can control if the encoder is increment or decrement clockwise by
changing the EXTI interrupt from one pin to the other or even easier
swap the connection of the encoder pins to the STM32.

Because most of the code is coming from my cmake template, then you
can ignore most of the code and focus on the the `tm1637.h` and
`tm1637.c` files and the functions that are referenced from the `main.c`.

## Why compare performance?
Well, the reason is that as expected the Arduino SDK libraries are bloated.
Which means that even on a very fast MCU like the STM32F303 that runs @72MHz,
when you turn the rotary encoder very fast, then there will be issues with
the code performance as the IRQ are too fast and the code execution is slow.

In order to test this you need to flash both firmwares and then test by
rotating the encoder knob fast.

## Components used
For this project I've used the `RobotDyn BlackPill F303CC`, a `KY-040`
compatible rotary encoder and the `RobotDyn 4-digit 7-segment TM1637`
module.

The encoder has the following pins (CLK, DT, SW, +, GND). The `SW` which
is the switch button is not used in this project. The `CLK` and the `DT`
are the two pins that you need to connect to the STM32's GPIOs and you
can swap those in order to control the clockwise increment/decrement.

## Pin connections
The following table shows the connections for the current code.

STM32F303 | KY-040 | TM1637
-|-|-
PB0 | - | CLK
PB1 | - | DT
PB9 | CLK | -
PB8 | DIO | -
GND | GND | GND
5V | - | 5V
3V3 | + | -

> Note: if the TM1637's 5V pin is not connected at 5V, but at 3V3 then
the display (in my case) works but it's more dim. Therefore, you can
either power the STM32 using the USB cable or if you're powering the
MCU using the ST-Link V2 (like in my case), then you can connect the
`5V` pin of the `TM1637` to the 5V output of the ST-Link.

## Enable tests
In order to use a debug GPIO to do performance test then uncomment the
`TEST1` or `TEST2` definitions in the `main.c` file.

* `TEST1`: Toggles the gpio pin to measure the time it takes to read the
`CLK` and `DT` pins of the `KY-040`.
* `TEST2`: Toggles the gpio pin to measure the time it takes for the whole
action of receiving the IRQ from the `KY-040` until the data are transmitted
to the `TM1637`.

## Build the code
You can build the code by clicking the `Build` task in the `PIO` activity
bar (that's the left bar, just click the alien icon to reveal the tasks).

## Flashing
To flash the code on the STM32 click the `Upload` action task in the PIO
activity bar.
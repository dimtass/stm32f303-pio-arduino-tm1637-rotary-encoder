#include <Arduino.h>
#include <TM1637Display.h>
#include "rotary_enc_pot.h"

// #define TEST1    // Toggle debug pin only when read the REA and REB in IRQ
// #define TEST2    // Toggle debug pin during the whole LED update

#if defined(TEST1) && defined(TEST2)
#error "Cannot define both TEST1 and TEST2"
#endif

// Module connection pins (Digital Pins)
#define CLK PB9
#define DIO PB8

#define REA PB0
#define REB PB1

TM1637Display display(CLK, DIO);
struct rep_pot pot1;
volatile bool read_pot = false;

int a = 0;
int b = 0;

void pot1_cbk(rep_val_t value)
{
	printf("POT1: %d\n", value);
    display.showNumberDec(value, true);  // Expect: 0000
}

void handleIRQ()
{
    if (!read_pot) {
        read_pot = true;
        #if defined(TEST1) || defined(TEST2)
        digitalWrite(PB10, 1);
        #endif
        a = digitalRead(REA);
        b = digitalRead(REB);
        #if defined(TEST1)
        digitalWrite(PB10, 0);
        #endif
    }
}


void setup() {
    // put your setup code here, to run once:
    Serial.begin(115200);
    printf("Program started\n");

    display.setBrightness(0x0f);
    display.showNumberDec(0, true);  // Expect: 0000

	rep_init(&pot1, 0, 0, 100, 1, 0, &pot1_cbk);

    pinMode(REA, INPUT_PULLUP);
    pinMode(REB, INPUT_PULLUP);
    attachInterrupt(REB, handleIRQ, FALLING);
    read_pot = false;

    pinMode(PB10, OUTPUT);
}


void loop() {
    // put your main code here, to run repeatedly:
    if (read_pot) {
        read_pot = false;
        rep_set_update_values(&pot1, digitalRead(REA), digitalRead(REB));
        #if defined(TEST2)
        digitalWrite(PB10, 0);
        #endif
    }
}